# RedBee Challenge

La idea es armar una arquitectura que en mayor o menor medida imite un diseño de microservicios, contemplando escalabilidad (horizontal) y alta disponibilidad. El entregable es/son el/los archivos de configuración .yaml, deployables en un cluster de Kubernetes, y en caso de armar tus propios servicios, el source.

Este sistema debe estar compuesto, mínimamente, por un frontend, un backend y un data store persistente (puede ser una DB). Todas las tecnologías son elegidas a piacere.

Los servicios pueden ser tan simples como se desee (nadie se va a quejar si el frontend está hecho con tablas, vainilla JS, y sin CSS), lo importante son las instancias y conexión entre ellas. Los servicios pueden ser mockeados, desarrollados custom, o reutilizados de otros proyectos, lo que te parezca mejor. El enfoque principal es ver cómo describís esa infra para que levante en k8s, te sugiero que no inviertas demasiado tiempo en los servicios en sí.

Podés usar Minikube, Tectonic (https://coreos.com/tectonic/sandbox), Rancher (en Vagrant: https://github.com/rancher/vagrant) o lo que te quede cómodo.

---------------

kubectl apply -f .